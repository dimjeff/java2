package day10teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day10TeamMembers {

    static HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();

    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("teams.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String[] data = line.split(":");
                if (data.length != 2) {
                    System.out.println("Indalied data: " + line);
                    continue;
                }
                String[] team = data[1].split(",");
                for (int i = 0; i < team.length; i++) {
                    if (playersByTeams.containsKey(team[i])) {
                        ArrayList<String> a = playersByTeams.get(team[i]);
                        a.add(data[0]);
                    } else {
                        ArrayList<String> a = new ArrayList<>();                        
                        a.add(data[0]);
                        playersByTeams.put(team[i], a);
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        readDataFromFile();
        for (Map.Entry<String, ArrayList<String>> entry : playersByTeams.entrySet()) {
            String player = entry.getKey();
            ArrayList<String> value = entry.getValue();
            System.out.printf("%s plays in: %s",player,String.join(", ", value));
            System.out.println("");
        }
//        for (String key : playersByTeams.keySet()) {
//            ArrayList<String> value = playersByTeams.get(key);
//            System.out.print(key+" plays in: ");
//            for (int i = 0; i < value.size(); i++) {
//                System.out.printf("%s%s",i==0?"":", ",value.get(i));
//            }
//            System.out.println("");
//        }
    }

}
