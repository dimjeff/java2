package day04namesdynamic;

import java.util.*;

public class Day04NamesDynamic {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("How many names you want to enter:");
        int total = input.nextInt();

        ArrayList<String> names = new ArrayList<>();
        //String[] names = new String[total];
        input.nextLine();

        for (int i = 0; i < total; i++) {
            System.out.print("Enter name #" + (i + 1) + ":");
            //names[i]= input.next();
            names.add(input.next());
        }
        //String pnames="";
        //pnames = Arrays.toString(names);
        //pname = String.join(", ", names);
        //pnames = pnames.replace("[", "").replace("]","");
        for (int i = 0; i < names.size(); i++) {
            //pnames += (i==0 ? "":", ") + names.get(i);
            System.out.printf("%s%s", i == 0 ? "" : ", ", names.get(i));
        }

        //System.out.println(pnames);
    }

}
