package testmaxmirror;

import java.util.Arrays;

public class TestMaxMirror {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] nums = {2, 7, 11, 15};
        int target=9;
        System.out.println(Arrays.toString(twoSum(nums,target)));
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] myNum = new int[2];
        for(int i=0;i<nums.length-1;i++)
            for(int j=0;j<nums.length;j++){
                if(i==j)
                    continue;
                if(nums[i]+nums[j]==target)
                {
                    myNum[0]=i;
                    myNum[1]=j;
                    return myNum;
                }                    
            }
        return myNum;
    }
}
