package day04namesplus;

import java.util.*;

public class Day04NamesPlus {
    public static void main(String[] args) {
        ArrayList<String> nameList = new ArrayList<>();
        
        Scanner input = new Scanner(System.in);
        
        String name;
        do{
            System.out.println("Please enter the name:");
            name = input.nextLine();
            if(name.equals(""))
                break;
            else
                nameList.add(name);
        } while (!name.equals(""));
        
        for (int i = 0; i < nameList.size(); i++) {
            System.out.printf("%s%s",i==0 ? "":"; ",nameList.get(i));
        } 
    }    
}
