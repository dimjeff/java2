package day08peopleagain;

import java.util.ArrayList;

class Person {

    public Person() {
        name = "Jimmy";
        age = 50;
    }

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    private String name;//length 2-50
    private int age;//1-150;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        if (name.length() < 2 || name.length() > 50) {
            throw new IllegalArgumentException("Name must be between 2-50 characters long.");
        }

        this.name = name;
    }

    public void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1-150.");
        }
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Person{" + "name=" + name + ", age=" + age + '}');
    }


}

class Student extends Person {

    private String program;
    private double gpa;

    public Student(String program, double gpa, String name, int age) {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }

    public void setGpa(double gpa) {
        if (gpa < 0.0 || gpa > 4.0) {
            throw new IllegalArgumentException("GPA must be between 0.0-4.0.");
        }
        this.gpa = gpa;
    }

    public void setProgram(String program) {
        if (program.length() < 2 || program.length() > 50) {
            throw new IllegalArgumentException("Program must be between 2-50 characters long.");
        }
        this.program = program;
    }

    public String getProgram() {
        return program;
    }

    public double getGpa() {
        return gpa;
    }

    @Override
    public String toString() {
        return String.format("Student name=" + getName() + ", age=" + getAge() + "program=" + program + ", gpa=" + gpa);
    }
}

class Teacher extends Person {

    String subject;
    int yearsOfExperience;

    public Teacher(String subject, int yearsOfExperience, String name, int age) {
        super(name, age);
        setSubject(subject);
        setYearsOfExperience(yearsOfExperience);
    }

    public void setSubject(String subject) {
        if (subject.length() < 2 || subject.length() > 50) {
            throw new IllegalArgumentException("subject must be between 2-50 characters long.");
        }
        this.subject = subject;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        if (yearsOfExperience < 0 || yearsOfExperience > 100) {
            throw new IllegalArgumentException("yearsOfExperience must be between 0-100.");
        }
        this.yearsOfExperience = yearsOfExperience;
    }

    @Override
    public String toString() {
        return String.format("Teacher{" + getName() + ", age=" + getAge() + "subject=" + subject + ", yearsOfExperience=" + yearsOfExperience + '}');
    }    
}

public class Day08PeopleAgain {

    static ArrayList<Person> peopleList = new ArrayList<>();

    public static void main(String[] args) {
        try {
            Person p1 = new Person("Jerry", 15);
            peopleList.add(p1);
            Person p2 = new Person("Jim", 30);
            peopleList.add(p2);
            Student s1 = new Student("IPD", 3.0, "Jeffrey", 23);
            peopleList.add(s1);
            Student s2 = new Student("IPD", 2.0, "Marry", 34);
            peopleList.add(s2);
            Teacher t1 = new Teacher("CS", 80, "Tom", 50);
            peopleList.add(t1);
            Teacher t2 = new Teacher("CS", 80, "John", 50);
            peopleList.add(t2);
            
            for(Person p : peopleList)
            {
                System.out.println(p);
            }
            
        } catch (IllegalArgumentException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

}
