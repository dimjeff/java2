package day06peopledualarray;

import java.util.*;

public class Day06PeopleDualArray {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("Please enter the number of people:");
            int size = input.nextInt();
            String[] namesArray = new String[size];
            int[] agesArray = new int[size];
            int youngest=0,youngerage = -1, tem;

            for (int i = 0; i < size; i++) {
                System.out.printf("Enter name of person #%d:", i + 1);
                namesArray[i] = input.next();
                System.out.printf("Enter age of person #%d:", i + 1);
                tem = input.nextInt();
                agesArray[i] = tem;
                if (youngerage == -1) 
                    youngerage = tem;

                if (tem < youngerage) 
                    youngerage = tem;
            }
            System.out.printf("Youngest person is %d and their name is(are) ",youngerage);
            for (int i = 0; i < size; i++) {
                if (youngerage == agesArray[i])
                    System.out.printf("%s ",namesArray[i]);
            }
            //System.out.printf("Youngest person is %d and their name is %s\n", agesArray[youngest], namesArray[youngest]);
        } catch (InputMismatchException ex) {
            System.out.printf("Got a error %s", ex.getMessage());
        }
    }

}
