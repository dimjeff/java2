package quiz2travel;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Quiz2Travel {
    static ArrayList<Trip> travelList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    
    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("travels.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    String[] data = line.split(";");
                    
                    if (data.length != 4) {
                        throw new InvalidParameterException("Error: invalid number of fields in line, skipping: "+line);
                        //continue;
                    }
                    String name = data[0];
                    if (name.length()<1 || name.length()>50) {
                        throw new InvalidParameterException("Error: traveller's name must be between 1-50 characters long, skipping: "+line);
                        //continue;
                    }
                    String city = data[1];
                    if (city.length()<1 || city.length()>50) {
                        throw new InvalidParameterException("Error: city must be between 1-50 characters long, skipping: "+line);
                        //continue;
                    }

                    String expectedPattern = "yyyy-MM-dd";
                    SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
                    Date departureDate = formatter.parse(data[2]);
                    Date returnDate = formatter.parse(data[3]);
                    if (!departureDate.before(returnDate)) {
                        throw new InvalidParameterException("Error: departure date must be before return date, skipping: "+line);
                    }
                    Trip newTrip = new Trip(name, city, departureDate, returnDate);
                    travelList.add(newTrip);
                } catch (InvalidParameterException|ParseException  ex) {
                    System.out.printf("Error parsing value:%s\n",ex.getMessage());
                    //continue;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }
    static int getMenuChoice() {
        int choice;
        try{
            System.out.print("Please choose an action:\n" +
                            "1. Display list of all travel plans using print()\n" +
                            "2. Add new Trip to the list from user input\n" +
                            "3. Find and display all trips that have not departed yet but will in the future\n" +
                            "4. Find and display the trip of the longest duration [(*) hard].\n" +
                            "0. Exit.\n");
            choice = input.nextInt();
            input.nextLine();
        }catch(InputMismatchException ex){
            choice = -1;
            input.nextLine();
        }
        return choice;
    }
    public static void main(String[] args) throws ParseException {
        readDataFromFile();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        while(true){
            int choice = getMenuChoice();
            if (choice == 0) {
                System.out.println("Good bye!");
                break; 
            }
            switch (choice) {
                case 1:{
                    for (Trip t : travelList) {
                        t.print();
                        //System.out.print(t.travellerName + " travels to "+ t.cityToVisit +" on "+ dateFormat.format(t.departureDate) +" and returns "+ dateFormat.format(t.returnDate) +"\n");
                    }
                }
                    break;
                case 2: 
                {
                    System.out.print("Please enter name:");
                    String name = input.nextLine();
                    if (name.length()<1 || name.length()>50) {
                        System.out.println("Error: traveller's name must be between 1-50 characters long");
                        continue;
                    }
                    System.out.print("Please enter city:");
                    String city = input.nextLine();
                    if (city.length()<1 || city.length()>50) {
                        System.out.println("Error: city must be between 1-50 characters long");
                        continue;
                    }
                    System.out.print("Please enter departure date:");
                    String departureDate = input.nextLine();                    
                    System.out.print("Please enter return date:");
                    String returnDate = input.nextLine();
                    Date dDate,rDate;
                    try{
                        String expectedPattern = "yyyy-MM-dd";
                        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
                        dDate = formatter.parse(departureDate);
                        rDate = formatter.parse(returnDate);
                        if (!dDate.before(rDate)) {
                            throw new InvalidParameterException("Error: departure date must be before return date ");
                    }
                    }catch(InvalidParameterException|ParseException  ex)
                    {
                        System.out.println("Error: wrong date format");
                        continue;
                    }
                    Trip newTrip = new Trip(name,city,dDate,rDate);
                    travelList.add(newTrip);
                }
                    break;
                case 3: {
                    System.out.println("The below trips that have not departed yet but will in the future:");
                    Date today = new Date();
                    for (Trip t : travelList) {
                        if(t.departureDate.after(today))
                            t.print();
                            //System.out.print(t.travellerName + " travels to "+ t.cityToVisit +" on "+ dateFormat.format(t.departureDate) +" and returns "+ dateFormat.format(t.returnDate) +"\n");
                    }
                }
                    break;
                case 4:{
                    if(travelList.size()==0){
                        System.out.println("No trip.");
                        break;
                    }
                        
                    Trip t = travelList.get(0);
                    int tDays = (int) ((t.returnDate.getTime() - t.departureDate.getTime()));
                    int longest=0;
                    for (int i = 0; i < travelList.size(); i++) {
                        int cDays = (int) ((travelList.get(i).returnDate.getTime() - travelList.get(i).departureDate.getTime()));
                        if(tDays<cDays)
                            longest = i;
                    }
                    System.out.println("The longest duration is: ");
                    travelList.get(longest).print();
                    
                }
                    break;
                default:
                    System.out.println("Error: invalid choice.");
            }
        }


    }
    
}

class Trip {
    String travellerName, cityToVisit;
    Date departureDate, returnDate;
    Trip(String travellerName,String cityToVisit,Date departureDate,Date returnDate){
        this.travellerName = travellerName;
        this.cityToVisit = cityToVisit;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
    }
    
    public void print() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        System.out.printf("%s travels to %s on %s and returns %s\n",this.travellerName,this.cityToVisit,dateFormat.format(this.departureDate),dateFormat.format(this.returnDate));
    }
}