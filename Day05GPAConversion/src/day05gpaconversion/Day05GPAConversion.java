package day05gpaconversion;

import java.io.*;
import java.util.*;

public class Day05GPAConversion {
    
    public static double total = 0.0;
    
    public static double mean (ArrayList<Double> table)
    {
        double total = 0.0;

        for ( int i= 0;i < table.size(); i++)
        {
            double currentNum = table.get(i);
            total+= currentNum;
        }
        return total/table.size();
    }

    public static double sd (ArrayList<Double> table)
    {
        double mean= mean(table);
        double temp =0;
        for ( int i= 0; i <table.size(); i++)
        {
            temp += Math.pow(table.get(i)-mean, 2);
        }
        temp = temp/table.size();
        
        return Math.sqrt(temp);
    }
    
    public static double convr(String str){
        double grd;
                switch (str) {
                    case "A":
                        grd = 4.0;
                        total+=4.0;
                        break;
                    case "A-":
                        grd= 3.7;
                        total+=3.7;
                        break;
                    case "B+":
                        grd= 3.3;
                        total+=3.3;
                        break;
                    case "B":
                        grd= 3.0;
                        total+=3.0;
                        break;
                    case "B-":
                        grd= 2.7;
                        total+=2.7;
                        break;
                    case "C+":
                        grd= 2.3;
                        total+=2.3;
                        break;
                    case "C":
                        grd= 2.0;
                        total+=2.0;
                        break;
                    case "D":
                        grd= 1.0;
                        total+=1.0;
                        break;
                    case "F":
                        grd= 0.0;
                        break;
                    default:
                        grd=-1;
                        //throw new IllegalArgumentException();
                }
        return grd;
    }
    
    public static void main(String[] args) {        
        int count = 0;
        int mid=0;
        ArrayList<Double> grades = new ArrayList<>();
        try ( Scanner fr = new Scanner(new File("C:\\Users\\6155181\\Documents\\java2\\Day05GPAConversion\\grades.txt"))) {
            String grade;

            while (fr.hasNextLine()) {
                grade = fr.nextLine();
                if (convr(grade)!=-1)
                    grades.add(convr(grade));
                count ++;
            }
            for (int i = 0; i < grades.size(); i++) {
                System.out.printf("%s%s",i==0 ? "":", ",grades.get(i));
            }
            System.out.printf("%nThe average is %.2f%n",mean(grades));
            mid = count/2;
            Collections.sort(grades);
            for (int i = 0; i < grades.size(); i++) {
                System.out.printf("%s%s",i==0 ? "":", ",grades.get(i));
            }
            System.out.printf("%nThe median is %.2f%n",grades.get(mid));
            
            System.out.printf("%nThe standard deviation is %.2f%n",sd(grades));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
