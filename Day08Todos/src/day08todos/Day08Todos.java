package day08todos;

import static day08todos.Todo.TaskStatus.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jdk.nashorn.api.tree.InstanceOfTree;

public class Day08Todos {

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static DateFormat dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
    static DateFormat dateFormatView = new SimpleDateFormat("yyyy/MM/dd");

    final static String fileName = "todos.txt";
    
//    static int inputInteger()
//    {
//        int num = inputInteger().
//    }

    static void loadDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File(fileName))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();

                    Todo newTodo = new Todo(line);
                    todoList.add(newTodo);
                } catch (IllegalArgumentException ex) {
                    System.out.printf("Error parsing value:%s\n", ex.getMessage());
                } catch (ParseException ex) {
                    System.out.printf("Error Date value:%s\n", ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    static void saveDataToFile() {
        try ( PrintWriter printWriter = new PrintWriter(new FileWriter("N"+fileName))) {
            for (int i = 0; i < todoList.size(); i++) {
                printWriter.println(todoList.get(i).toDataString());
            }
            System.out.printf("Saved todo list successfully.\n");
            System.out.println("");
        } catch (IOException ex) {
            System.out.println("Error writing to file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        loadDataFromFile();
        while (true) {
            int choice = getMenuChoice();
            if (choice == 0) {
                System.out.println("");
                System.out.println("Exiting. Good bye!");
                break;
            }
            switch (choice) {
                case 1:
                    addTodo();
                    break;
                case 2:
                    listAllTodos();
                    break;
                case 3:
                    deleteTodo();
                    break;
                case 4:
                    modifyTodo();
                    break;
                case 5:
                    saveDataToFile();
                    break;
                default:
                    System.out.println("Invalid integer, please try again:");
                    System.out.println("");
            }
        }
    }

    static int getMenuChoice() {
        int choice;
        try {
            System.out.print("Please make a choice [0-4]:\n"
                    + "1. Add a todo\n"
                    + "2. List all todos (numbered)\n"
                    + "3. Delete a todo\n"
                    + "4. Modify a todo\n"
                    + "5. Save to file\n"
                    + "0. Exit\n"
                    + "Your choice is:");
            choice = input.nextInt();
        } catch (InputMismatchException ex) {
            choice = -1;            
        }
        finally{
            input.nextLine();
        }
        return choice;
    }

    private static void addTodo() {
        System.out.println("");
        System.out.println("Adding a todo.");

        try {
            Todo td = AddNewTodo();
            todoList.add(td);
            System.out.println("You've created the following todo:");
            System.out.println(td);
        } catch (ParseException ex) {
            System.out.printf("Error: %s\n", ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.println("wrong enter valid number");
        } catch (IllegalArgumentException ex) {
            System.out.printf("Error: %s\n", ex.getMessage());
        }
        System.out.println("");
    }

    private static void listAllTodos() {
        System.out.println("");
        System.out.println("Listing all todos.");
        for (int i = 1; i <= todoList.size(); i++) {
            System.out.printf("#%d: %s\n", i, todoList.get(i - 1));
        }
        System.out.println("");
    }

    private static void deleteTodo() {
        System.out.println("");
        System.out.print("Deleting a todo. Which todo # would you like to delete? ");
        int num = -1;
        try {
            num = input.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Error: invalid number.");
            System.out.println("");
            return;
        }
        if (num >= todoList.size() || num < 0) {
            System.out.println("Error: invalid number.");
        } else {
            todoList.remove(num - 1);
            System.out.printf("Deleted todo #%d successfully.\n", num);
        }
        System.out.println("");
    }

    private static Todo AddNewTodo() throws ParseException {
        System.out.print("Enter task description:");
        String task = input.nextLine();
        System.out.print("Enter due Date (yyyy/mm/dd):");
        String dueDate = input.nextLine();
        Date dDate = new Date();
        Todo.TaskStatus status = Pending;
        int hoursOfWork = -1;
        try {
            dDate = dateFormatView.parse(dueDate);
            System.out.print("Enter hours of work (integer):");
            hoursOfWork = input.nextInt();
            input.nextLine();
        } catch (InputMismatchException ex) {
            ex.printStackTrace();
            input.nextLine();
            throw new InputMismatchException("A worng number of hours of work. ");
        }
        Todo td = new Todo(task, dDate, hoursOfWork, status);

        return td;
    }

    private static void modifyTodo() {
        System.out.println("");
        System.out.print("Modifying a todo. Which todo # would you like to modify? ");
        int num = -1;
        try {
            num = input.nextInt();
            input.nextLine();
        } catch (InputMismatchException ex) {
            input.nextLine();
            System.out.println("Error: invalid number.");
            System.out.println("");
            return;
        }
        if (num > todoList.size() || num <= 0) {
            System.out.println("Error: does not have this number.");
        } else {
            System.out.printf("Modifying todo #%d: %s\n", num, todoList.get(num - 1));

            try {
                //Todo td = AddNewTodo();

                System.out.print("Enter task description:");
                String task = input.nextLine();
                System.out.print("Enter due Date (yyyy/mm/dd):");
                String dueDate = input.nextLine();
                Date dDate = new Date();
                Todo.TaskStatus status = Pending;
                int hoursOfWork = -1;
                try {
                    dDate = dateFormatView.parse(dueDate);
                    System.out.print("Enter hours of work (integer):");
                    hoursOfWork = input.nextInt();
                    input.nextLine();
                } catch (InputMismatchException ex) {
                    input.nextLine();
                    throw new InputMismatchException("A worng number of hours of work. ");
                }
                System.out.print("Enter status:");
                String staString = input.nextLine();
                
                status = Todo.TaskStatus.valueOf(staString);
                
                Todo td = new Todo(task, dDate, hoursOfWork, status);

                todoList.set(num - 1, td);
                System.out.printf("You've modified todo #%d as follows:\n", num);
                System.out.println(td);
            } catch (ParseException | InputMismatchException | IllegalArgumentException ex) {
                System.out.printf("Error: Enter a worng data: %s\n", ex.getMessage());
            }
        }
        System.out.println("");
    }

}

class Todo {

    String task; // 2-50 characters long
    Date dueDate; // Date between year 1900 and 2100
    int hoursOfWork; // 0 or greater number

    enum TaskStatus {
        Pending, Done
    };
    TaskStatus status;

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public Todo(String dataLine) throws NumberFormatException, ParseException {
        if (!dataLine.contains(";")) {
            throw new IllegalArgumentException("The string must contain a semicolon in line,skipping :" + dataLine);
        }
        String[] strArray = dataLine.split(";");
        if (strArray.length != 4) {
            throw new IllegalArgumentException("The string must follow the rules.,skipping :" + dataLine);
        }
        int hours = Integer.valueOf(strArray[2]);
        Date dueDate = Day08Todos.dateFormatFile.parse(strArray[1]);

        setTask(strArray[0]);
        setDueDate(dueDate);
        setHoursOfWork(hours);
        setStatus(TaskStatus.valueOf(strArray[3]));
    }

    public Todo(String task, Date dueDate, int hoursOfWork, TaskStatus status) {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
        setStatus(status);
    }

    public String getTask() {
        return task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setTask(String task) {
        if (task.length() < 2 || task.length() > 50) {
            throw new IllegalArgumentException("Task must be between 2-50 characters long.");
        }
        String regEx = "[;|\\']+"; //website regex101.com
        //Pattern pattern = Pattern.compile(regEx);
        //Matcher matcher = pattern.matcher(task);

        //if(matcher.find()) {
        if (task.matches(regEx)) {
            throw new IllegalArgumentException("Task can not contain a semicolon or | or `.");
        }

        this.task = task;
    }

    public void setDueDate(Date dueDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dueDate);
        int year = calendar.get(Calendar.YEAR);
        if (year < 1900 || year > 2100) {
            throw new IllegalArgumentException("Date between year 1900 and 2100");
        }
        this.dueDate = dueDate;
    }

    public void setHoursOfWork(int hoursOfWork) {
        if (hoursOfWork < 0) {
            throw new IllegalArgumentException("Hours of work must be greater zero.");
        }
        this.hoursOfWork = hoursOfWork;
    }

    // format all fields of this Todo item for display exactly as specified below in the example interactions
    @Override
    public String toString() {
        return String.format("%s, %s, will take %d hour(s) of work is %s", this.task, Day08Todos.dateFormatView.format(this.dueDate), this.hoursOfWork, this.status);
    }

    public String toDataString() {
        return String.format("%s;%s;%d;%s", this.task, Day08Todos.dateFormatFile.format(this.dueDate), this.hoursOfWork, this.status);
    }
}
