package day10numberfrequency;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Day10NumberFrequency {

    static ArrayList<Integer> intArr = new ArrayList<>();
    static ArrayList<IntegerSort> sortArr = new ArrayList<>();

    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("integers.txt"))) {
            while (fileInput.hasNextLine()) {//hasNextInt() can not handle char
//                try{
//                    intArr.add(fileInput.nextInt());
//                }catch(InputMismatchException ex)
//                {
//                    System.out.println(""+ ex.getMessage());
//                }catch(NoSuchElementException ex)
//                {
//                    System.out.println(""+ ex.getMessage());
//                }
                String line = fileInput.nextLine();
                String[] data = line.split(" ");
                for (int i = 0; i < data.length; i++) {
                    try {
                        intArr.add(Integer.parseInt(data[i]));
                    } catch (NumberFormatException ex) {
                        System.out.printf("Error integer value:%s\n", ex.getMessage());
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        readDataFromFile();
        Collections.sort(intArr);
        System.out.println(intArr.toString());
        //[-9, -7, -2, -1, 0, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 5, 6, 7, 7, 8, 8, 9, 193]
        int i = 0;
        while (i < intArr.size()) {
            int start = intArr.indexOf(intArr.get(i));
            int end = intArr.lastIndexOf(intArr.get(i));
            IntegerSort is = new IntegerSort(intArr.get(i), end + 1 - i);
            sortArr.add(is);
            i = end + 1;
        }
        Collections.sort(sortArr,IntegerSort.SortNum);
        if(!sortArr.isEmpty())
            System.out.println("Number | Occurrences \n-------+------------");
        for (IntegerSort is : sortArr)
            System.out.println(is);
    }
}

class IntegerSort implements Comparable<IntegerSort> {
    int num;
    int count;

    public IntegerSort(int num, int count) {
        this.num = num;
        this.count = count;
    }
    
    static Comparator<IntegerSort> SortNum = new Comparator<>() {
        public int compare(IntegerSort i1, IntegerSort i2) {
            if(i1.count-i2.count != 0)
                return i2.count-i1.count;
            else
                return i1.num-i2.num;
        }
    };

    @Override
    public String toString() {
        //return String.format("%6s |%11s", num, count);
        return String.format("%-6d |%11d", num, count);
    }

    @Override
    public int compareTo(IntegerSort arg0) {
        return arg0.count-count;
    }

}
