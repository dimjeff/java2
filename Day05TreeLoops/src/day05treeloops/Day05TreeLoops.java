package day05treeloops;

import java.util.Scanner;

public class Day05TreeLoops {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("How big does your tree needs to be? ");
        int count = input.nextInt();
        
        int max = count+count-1;
        int mid = max/2+1;
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < max; j++) {
                if(j>=mid-i-1 && j<=mid+i-1)
                    System.out.print("*");
                else
                    System.out.print(" ");
                if(j==max-1)
                    System.out.print("\n");
            }
        }
    }
    
}
