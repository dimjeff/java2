package day04numbers;
import java.util.*;
public class Day04Numbers {

    public static void main(String[] args) {
        // TODO code application logic here
         Scanner input = new Scanner(System.in);
        System.out.print("How many names you want to generate:");
        int size=input.nextInt();
        ArrayList<Integer> numsList = new ArrayList<>();
        input.nextLine();
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            numsList.add(r.nextInt(201)-100);
        }
        for (int n : numsList) {
            if (n<=0)
                System.out.println(n);
        }
    }
    
}
