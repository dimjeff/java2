package day05arraycontains;

import java.util.Arrays;

public class Day05ArrayContains {
    public static int[] concatenate(int[] a1, int[] a2) { 
        int l = a1.length+a2.length;
        int[] myIntArray = new int[l];
        System.arraycopy(a1, 0, myIntArray, 0, a1.length);
        System.arraycopy(a2, 0, myIntArray, a1.length, a2.length);
        return myIntArray;
    }
    
    public static void printDups(int[] a1, int a2[]) {
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if(a1[i]==a2[j])
                {
                    System.out.println(a1[i]);
                    break;
                }
            }
        }
    }
    
    public static int[] removeDups(int[] a1, int[] a2) { 
        int[] myIntArray;
        String str="";
        boolean isIn;
        for (int i = 0; i < a1.length; i++) {
            isIn=false;
            for (int j = 0; j < a2.length; j++) {
                if(a1[i]==a2[j])
                {
                    isIn = true;
                    break;
                }
            }
            if(!isIn)
                str += a1[i]+",";
        }
        if(str.charAt(str.length()-1)==',')
            str = str.substring(0,str.length()-1);
        String[] arrStr = str.split(",");
        myIntArray = new int[arrStr.length];
        for (int i = 0; i < arrStr.length; i++) {
            myIntArray[i] = Integer.valueOf(arrStr[i]);
        }
        return myIntArray;
    }
    
    public static void main(String[] args) {
        int[] a1={1, 3, 7, 8, 2, 7, 9, 11};
        int[] a2={3, 8, 7, 5, 13, 5, 12, 3};
        int[] a3=concatenate(a1,a2);
        System.out.println(Arrays.toString(a3));
        printDups(a1,a2);
        int[] a4=removeDups(a1,a2);
        System.out.println(Arrays.toString(a4));
    }
}
