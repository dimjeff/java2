package day04simplecalc;
import java.util.*;
public class Day04SimpleCalc {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num=0;
        while (true){
            System.out.print("Please choose an option from menu:\n" +
                            "1. Add\n" +
                            "2. Subtract\n" +
                            "3. Multiply\n" +
                            "4. Divide\n" +
                            "0. Exit\n");

            num = input.nextInt();
            if(num==0)
                break;
            
            if (num!=1 && num!=2 && num!=3 && num!=4)
            {
                System.out.println("Error: got the wrong number.");
                break;
            }
            input.nextLine();
            System.out.println("Pleaes enter 2 floating point values:");
            float f1=0,f2=0,result=0;
            f1=input.nextFloat();
            f2=input.nextFloat();
            switch (num){
                case 1:
                    result = f1+f2;
                    break;
                case 2:
                    result = f1-f2;
                    break;
                case 3:
                    result = f1*f2;
                    break;
                case 4:
                    result = f1/f2;
                    break;
                default:
                    System.out.println("Error: invalid status");
                    System.exit(1);
            }
            System.out.printf("The result of 2 numbers is %.2f%n",result);
        }         
    }
    
}
