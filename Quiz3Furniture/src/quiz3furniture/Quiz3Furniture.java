package quiz3furniture;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Quiz3Furniture {
    static ArrayList<HomeItem> homeItemsList = new ArrayList<>();
    
    public static void main(String[] args) {
        //Read data from file and Display all HomeItems using toString, one per line
        readDataFromFile();
        System.out.println("");
        
        //Display the total number of HomeItems created
        System.out.println("The total object = "+HomeItem.getTotalCreated());
        
        //Sort and display all HomeItems according to price
        System.out.println("");
        System.out.println("Sort and display all HomeItems according to price");
        Collections.sort(homeItemsList);
        for (HomeItem h : homeItemsList) {
            System.out.println(h);
        }
        
        //Sort and display all HomeItems according to price and desription
        System.out.println("");
        System.out.println("Sort and display all HomeItems according to price and desription");
        Collections.sort(homeItemsList,HomeItem.SortPriceDesc);
        for (HomeItem h : homeItemsList) {
            System.out.println(h);
        }
        
        //Sort and display only Electronics items ordered by their serial number.
        System.out.println("");
        System.out.println("Sort and display only Electronics items ordered by their serial number");
        Collections.sort(homeItemsList,HomeItem.SortSerialNo);
        for (HomeItem h : homeItemsList) {
            if(h.getClass().getSimpleName().equals("ElectronicsItem"))
                System.out.println(h);
        }
        
        saveDataToFile();
    }    

    static void saveDataToFile() {
        try ( PrintWriter fileOutput = new PrintWriter(new File("sorted.txt"))) {
            Collections.sort(homeItemsList,HomeItem.SortPriceDesc);
            for (HomeItem h : homeItemsList) {
                fileOutput.println(h.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing file: " + ex.getMessage());
        }
    }

    
    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("household.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    HomeItem itemObj;
                    itemObj = HomeItem.createFromDataLine(line);
                    homeItemsList.add(itemObj);
                    System.out.println(itemObj);
                } catch (DataInvalidException ex) {
                    System.out.println("Error parsing line: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
        
    }
}

class DataInvalidException extends RuntimeException {
    public DataInvalidException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DataInvalidException(String msg) {
        super(msg);
    }
}

interface Marshallable {
    String toDataString();
}

abstract class HomeItem implements Marshallable,Comparable<HomeItem> {
    private int SerialNo;
    public static int totalObj = 0;
    private String description; // 2-50 characters except semicolon ;
    private double price; // 0-999999 floating point

    public HomeItem(String description, double price) {
        setDescription(description);
        setPrice(price);
        totalObj++;
        SerialNo = totalObj;
    }
    
    @Override
    public int compareTo(HomeItem h) {
        return price>h.price?1:-1;
    }
    
    static Comparator<HomeItem> SortPriceDesc = new Comparator<HomeItem>() {
        @Override
        public int compare(HomeItem h1, HomeItem h2) {
            if (h1.price < h2.price) {
                return -1;
            }
            if (h1.price > h2.price) {
                return 1;
            } else {
                return h1.description.compareTo(h2.description);
            }
        }
    };
    
    static Comparator<HomeItem> SortSerialNo = new Comparator<HomeItem>() {
        @Override
        public int compare(HomeItem h1, HomeItem h2) {
                return h1.getSerialNo() - h2.getSerialNo();
        }
    };
    
    static HomeItem createFromDataLine(String line) {
        try {
            String[] data = line.split(";");
            switch (data[0]) {
                case "Furniture": {
                    if (data.length != 4) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + line);
                    }
                    double price;
                    try {
                        price = Double.parseDouble(data[1]);
                    } catch (NumberFormatException ex) {
                        throw new DataInvalidException("Error: invalid price in line: " + line);
                    }                    
                    String desc = data[2];
                    FurnitureItem.HomeItemType itemType;
                    try{
                        itemType = FurnitureItem.HomeItemType.valueOf(data[3]);
                    }
                    catch(IllegalArgumentException ex)
                    {
                        throw new DataInvalidException("Error: invalid HomeItemType in line: " + line);
                    }
                    return new FurnitureItem(price, desc, itemType);
                }
                case "Electronics": {
                    if (data.length != 4) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + line);
                    }
                    double price;
                    try {
                        price = Double.parseDouble(data[1]);
                    } catch (NumberFormatException ex) {
                        throw new DataInvalidException("Error: invalid price in line: " + line);
                    }  
                    String desc = data[2];
                    String model = data[3];
                    return new ElectronicsItem(price, desc, model);
                }
                default:
                    throw new DataInvalidException("Error: don't know what it is " + line);
            }
        } catch (NumberFormatException ex) {
            throw new DataInvalidException("Parsing error", ex);
        }
    } // factory method

    static int getTotalCreated() {//
        return totalObj;
    }

    public void setDescription(String description) {
        if(!description.matches("[^;]{2,50}$"))
            throw new DataInvalidException("Error: invalid item description:" + description);
        this.description = description;
    }

    public void setPrice(double price) {
        if(price<0 || price>999999)
            throw new DataInvalidException("Error: invalid item price:" + price);
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getSerialNo() {
        return SerialNo;
    }
    
    @Override
    public String toString() {
        //return "HomeItem{" + "getSerialNo=" + getSerialNo + ", description=" + description + ", price=" + price + '}';
        return String.format("%s: SerialNo=%d, description=%.2f, price=%.2f",
                        getClass().getSimpleName(),getSerialNo(), getDescription(), getPrice());
    } 
}

class FurnitureItem extends HomeItem {
    private HomeItemType itemType;
    enum HomeItemType {
        Desk, Chair, Table, Sofa, Bed
    }
    public FurnitureItem(double price, String description, HomeItemType itemType) {
        super(description, price);
        setItemType(itemType);
    }
    
    public void setItemType(HomeItemType itemType) {
        this.itemType = itemType;
    }

    @Override
    public String toDataString(){
        //Furniture;890.99;brown comfy sofa;Sofa
        return String.format("%s;%.2f;%s;%s", getClass().getSimpleName().replace("Item", ""),super.getPrice(),super.getDescription(),getItemType());
    }
    

    public HomeItemType getItemType() {
        return itemType;
    }

    @Override
    public String toString() {
        //return "FurnitureItem{" + "itemType=" + itemType + '}';
        return String.format("%s: SerialNo=%d, description=%s, price=%.2f, itemType=%s",
                        getClass().getSimpleName(),super.getSerialNo(), super.getDescription(), super.getPrice(),getItemType());
    }
}

class ElectronicsItem extends HomeItem {

    private String model; // 2-50 characters except semicolon ;
    
    public ElectronicsItem(double price, String description, String model) {
        super(description, price);
        setModel(model);
    }

    public void setModel(String model) {
        if(!model.matches("[^;]{2,50}$"))
            throw new DataInvalidException("Error: invalid model:" + model);
        this.model = model;
    }

    @Override
    public String toDataString(){
        //Electronics;890.99;alarm clock;HH678
        return String.format("%s;%.2f;%s;%s", getClass().getSimpleName().replace("Item", ""),super.getPrice(),super.getDescription(),getModel());
    }

    @Override
    public String toString() {
        //return "ElectronicsItem{" + "model=" + model + '}';
        return String.format("%s: SerialNo=%d, description=%s, price=%.2f, model=%s",
                        getClass().getSimpleName(),super.getSerialNo(), super.getDescription(), super.getPrice(),getModel());
    }

    public String getModel() {
        return model;
    }    
    
}
