package day06randnums;

import java.util.*;

public class Day06RandNums {

    public static void main(String[] args) {
	RandomStore rs = new RandomStore();
	int v1 = rs.nextInt(1, 10);
	int v2 = rs.nextInt(-100, -10);
	int v3 = rs.nextInt(-20,21);
	System.out.printf("Values %d, %d, %d\n", v1, v2, v3);
	rs.printHistory();
    }
}

class RandomStore {
    ArrayList<Integer> intHistory = new ArrayList<>();

    int nextInt(int minIncl, int maxExcl) {
        Random r = new Random();
        int t = r.nextInt(maxExcl-minIncl)+minIncl;
        intHistory.add(t);
        return t;
    }

    void printHistory() {
        for (int i = 0; i < intHistory.size(); i++) {
            System.out.printf("%s%d",i==0 ? "": ", ",intHistory.get(i));
        }
        System.out.println("");
    }
}
